﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    Rigidbody rb;
    bool isGround;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        isGround = false;
    }

    void OnCollisionEnter(Collision collision)
    {
        isGround = true;
    }

	// Update is called once per frame
	void Update () {
        if (GameSystem.slCon.isControlled())
        {
            float move = GameSystem.slCon.mX / GameSystem.ctrlSize * 0.1f;
            transform.Translate(move, 0, 0);
        }
        if (GameSystem.slCon.isActioned())
        {
            switch (GameSystem.slCon.actionState)
            {
                case 0:
                    break;
                case 1: // Left
                    break;
                case 2: // Right
                    break;
                case 3: // Up
                    Jump();
                    break;
                case 4: // Down
                    break;
                default:
                    break;
            }
        }
	}

    void Jump()
    {
        if (isGround)
        {
            rb.AddForceAtPosition(Vector3.up * 30000, transform.position);
            isGround = false;
        }
    }
}
