﻿using UnityEngine;
using System.Collections;

public class SlideController
{

    public float fX, fY, cX, cY, mX, mY, x0, y0;
    private const int ctrlSize = 70;
    int hwidth, hheight, actionID, controllID;
    public int actionState;
    bool actionable;

    // Use this for initialization
    public SlideController(int c)
    {
        c = ctrlSize;
        fX = fY = cX = cY = -1;
        mX = mY = 0;
        hwidth = (int)((float)Screen.width * 0.5f);
        hheight = (int)((float)Screen.height * 0.5f);
        actionID = controllID = -1;
        actionState = -1;
        actionable = true;
    }

    // Update is called once per frame
    public void Update()
    {
        foreach (Touch touch in Input.touches)
        {
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (touch.position.x < hwidth && controllID < 0)
                    {
                        controllID = touch.fingerId;
                        cX = touch.position.x - hwidth;
                        fX = touch.position.x - hwidth;
                        cY = touch.position.y - hheight;
                        fY = touch.position.y - hheight;
                        mX = 0;
                        mY = 0;
                    }
                    else if (touch.position.x >= hwidth && actionID < 0)
                    {
                        actionID = touch.fingerId;
                        actionState = 0;
                        x0 = touch.position.x - hwidth;
                        y0 = touch.position.y - hheight;
                    }
                    break;
                case TouchPhase.Moved:
                    if (touch.fingerId == controllID)
                    {
                        mX = touch.position.x - hwidth - cX;
                        mY = touch.position.y - hheight - cY;
                        if (mX * mX + mY * mY > ctrlSize * ctrlSize) {

                            double rad = System.Math.Atan(mY / mX), sign = mX / System.Math.Abs(mX);
                            mX = (float)((float)sign * (ctrlSize * System.Math.Cos(rad)));
                            mY = (float)((float)sign * (ctrlSize * System.Math.Sin(rad)));
                        }
                        fX = mX + cX;
                        fY = mY + cY;
                    }
                    if (touch.fingerId == actionID && actionable)
                    {
                        double x = touch.position.x - hwidth - x0,
                               y = touch.position.y - hheight - y0,
                               r = System.Math.Sqrt(x * x + y * y);
                        if (r > 5)
                        {
                            double rad = System.Math.Atan(y / x);
                            if (rad > -System.Math.PI / 4 && rad <= System.Math.PI / 4)
                            {
                                if (x > 0)
                                {
                                    // RIGHT
                                    actionState = 2;
                                }
                                else
                                {
                                    // LEFT
                                    actionState = 1;
                                }
                            }
                            else
                            {
                                if (y > 0)
                                {
                                    // UP
                                    actionState = 3;
                                }
                                else
                                {
                                    // DOWN
                                    actionState = 4;
                                }
                            }
                            actionable = false;
                        }
                    }
                    else
                    {
                        actionState = 5;
                    }
                    break;
                case TouchPhase.Ended:
                    if (touch.fingerId == controllID)
                    {
                        controllID = -1;
                        cX = fX = cY = fY = -1;
                        mX = mY = 0;
                    }
                    if (touch.fingerId == actionID)
                    {
                        actionID = -1;
                        actionState = -1;
                        actionable = true;
                    }
                    break;
                default:
                    break;
            }
        }
    }

    public bool isControlled()
    {
        if (controllID < 0) return false;
        return true;
    }

    public bool isActioned()
    {
        if (actionID < 0) return false;
        return true;
    }
}
