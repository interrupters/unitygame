﻿using UnityEngine;
using System.Collections;

public class GameSystem : MonoBehaviour {

    public static SlideController slCon;
    public static int ctrlSize = 50;

    // Use this for initialization
	void Start () {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        slCon = new SlideController(ctrlSize);
	}
	
	// Update is called once per frame
	void Update () {
        slCon.Update();
	}
}
