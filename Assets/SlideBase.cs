﻿using UnityEngine;
using System.Collections;

public class SlideBase : MonoBehaviour {
    bool pre;
	// Use this for initialization
	void Start () {
        Vector3 pos = new Vector3(Screen.width, Screen.height, 0);
        transform.localPosition = pos;
        pre = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (GameSystem.slCon.isControlled())
        {
            float x = GameSystem.slCon.cX,
                  y = GameSystem.slCon.cY;
            Vector3 pos = new Vector3(x, y, 0);
            transform.localPosition = pos;
            //GetComponent<Canvas>().enabled = true;
            pre = true;
        }
        else if (pre)
        {
            Vector3 pos = new Vector3(Screen.width, Screen.height, 0);
            transform.localPosition = pos;
            pre = false;
        }
    }
}
